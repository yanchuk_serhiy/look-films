import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/random',
      name: 'random',
      component: () => import(/* webpackChunkName: "about" */ './views/Random.vue')
    },
    {
      path: '/:slug',
      name: 'film',
      component: () => import(/* webpackChunkName: "about" */ './views/FilmView.vue')
    }
  ],
  scrollBehavior () {
    return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ x: 0, y: 0 })
    }, 500)
  })
  }
})
